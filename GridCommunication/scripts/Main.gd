extends Node2D

func _ready():
	var grid = Globals.size #MayBeChanged
	create_grid(grid)
	set_camera(grid)
	change_tile_status(Globals.startingPoint)

func create_grid(grid:Vector2) -> void :
	var tileObject = preload("res://prefabs/Tile.tscn")
	for xPos in range (grid.x):
		for yPos in range (grid.y):
			var tileInstance = tileObject.instance()
			tileInstance.translate(Vector2(xPos*10, yPos*10))
			$Tiles.add_child(tileInstance)

func set_camera(grid:Vector2) -> void :
#0.1 in camera = 10x6
	var xCameraZoom = (grid.x/10) *0.1
	var yCameraZoom = (grid.y/6) *0.1
#both zoom need to be equal to keep ratio
	$Camera.zoom = Vector2(max(xCameraZoom, yCameraZoom),max(xCameraZoom, yCameraZoom))
	$Camera.position = Vector2((grid.x/2)*10, (grid.y/2)*10)



func change_tile_status(position:Vector2):
# warning-ignore:narrowing_conversion
	if ($Tiles.get_child(position.x*Globals.size.y + position.y).switchSate()):
		newTiles.append(position) #if the tile is new, it will be used



var spreadingTiles:Array = [Globals.startingPoint]
var newTiles:Array = []

# warning-ignore:unused_argument
func _physics_process(delta):
	if (Input.is_action_just_pressed("ui_accept")):
		for tile in spreadingTiles:
			spread(tile, Vector2(3,-2))
			spread(tile, Vector2(-3,-1))
			spread(tile, Vector2(1,2))
			$Tiles.get_child(tile.x*Globals.size.y + tile.y).validate()
		spreadingTiles = newTiles
		newTiles = []

func spread(tile, movement):
	if (tile.x + movement.x >= 0 && tile.x + movement.x < Globals.size.x &&
		tile.y + movement.y >= 0 && tile.y + movement.y < Globals.size.y):
		change_tile_status(tile+movement)
