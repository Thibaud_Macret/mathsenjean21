extends Node2D

var state:bool = false

func switchSate() ->bool: #returns the state before
	if(state):
		return false
	else :
		state = true
		$Inner.color = Color(1, 0, 0)
	return true

func validate() : #switch color
	$Inner.color = Color(0.55, 0, 0, 1)
