extends Node2D

func _on_sizeTxt_text_changed():
	if !($xSizeSelector/xSizeTxt.text == String(int($xSizeSelector/xSizeTxt.text))):
		$xSizeSelector/xSizeTxt.text = String(int($xSizeSelector/xSizeTxt.text))

func _on_ySizeTxt_text_changed():
	if !($ySizeSelector/ySizeTxt.text == String(int($ySizeSelector/ySizeTxt.text))):
		$ySizeSelector/ySizeTxt.text = String(int($ySizeSelector/ySizeTxt.text))



func _on_xBeginingPoint_text_changed():
	if !($xBeginingPoint/xBeginingPointTxt.text == String(int($xBeginingPoint/xBeginingPointTxt.text))):
		$xBeginingPoint/xBeginingPointTxt.text = String(int($xBeginingPoint/xBeginingPointTxt.text))
	if(int($xBeginingPoint/xBeginingPointTxt.text) >= int($xSizeSelector/xSizeTxt.text)):
		$xBeginingPoint/xBeginingPointTxt.text = String(int($xSizeSelector/xSizeTxt.text) - 1)
	if(int($xBeginingPoint/xBeginingPointTxt.text) < 0):
		$xBeginingPoint/xBeginingPointTxt.text = '0'

func _on_yBeginingPoint_text_changed():
	if !($yBeginingPoint/yBeginingPointTxt.text == String(int($yBeginingPoint/yBeginingPointTxt.text))):
		$yBeginingPoint/yBeginingPointTxt.text = String(int($yBeginingPoint/yBeginingPointTxt.text))
	if(int($yBeginingPoint/yBeginingPointTxt.text) >= int($ySizeSelector/ySizeTxt.text)):
		$yBeginingPoint/yBeginingPointTxt.text = String(int($ySizeSelector/ySizeTxt.text) - 1)
	if(int($yBeginingPoint/yBeginingPointTxt.text) < 0):
		$yBeginingPoint/yBeginingPointTxt.text = '0'



func _on_BtnGo_pressed():
	Globals.size.x = int($xSizeSelector/xSizeTxt.text)
	Globals.size.y = int($ySizeSelector/ySizeTxt.text)
	Globals.startingPoint.x = int($xBeginingPoint/xBeginingPointTxt.text)
	Globals.startingPoint.y = int($yBeginingPoint/yBeginingPointTxt.text)
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://scenes/Main.tscn")
