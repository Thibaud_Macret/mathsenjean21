# MathsEnJean 2021 - 2022

## Sujet : 
N°10 : Communiquer dans une grille
Les personnes d’un réseau sont disposées dans un quadrillage a taille variable. Les règles de
transmission du réseau sont au nombre de trois :
- règle A : Transmettre à la personne située 3 pas à l’est et 2 pas au nord
- règle B : Transmettre à la personne située 3 pas à l’ouest et 1 pas au nord
- règle C : Transmettre à la personne située 1 pas à l’est et 2 pas au sud
Une nouvelle, connue d'une personne particulière peut-elle être transmise à tout le monde en
suivant les règles du réseau ? Sinon, quelles sont les personnes qui peuvent être informées ?

## Documents :
Premières réflexions : 
- https://docs.google.com/document/d/1zdr6-RhIT7QU44_A2CpdvCbbwOdYkTfjezWpoJQGM4g/edit?usp=sharing

## Documentation de l'appli :
WIP